
case $1 in
  start)
    docker network create dev-network
    docker pull mysql/mysql-server:8.0
    docker pull php:7.2-apache
    mkdir config
    mkdir config/mysql_data
    mkdir config/mysql_data/8.0
    
    docker rm local-mysql
    docker rm local-dev-php
    docker run --name local-mysql --net dev-network -v $PWD/config/mysql_data/8.0/:/var/lib/mysql -p 3306:3306 -d -e MYSQL_ROOT_PASSWORD=w0rd10p@ss mysql:8.0
    docker run --name local-dev-php --net dev-network -v $PWD/:/var/www/html -p 80:80 -d php:7.2-apache /bin/bash -c 'a2enmod rewrite; apache2-foreground'
    docker ps
    ;;

  end)
    docker stop $(docker ps -a -q)
    docker ps
    ;;

  logs)
    echo "-----------------------------------------------------"
    echo "PHP Version               : 7.2"
    echo "MYSQL Version             : 8.0"
    echo "MYSQL DEFAULT PASSWORD    : w0rd10p@ss"
    echo "Webroot PATH              : $PWD/Develop/webroot"
    echo "-----------------------------------------------------"
    ;;
esac





