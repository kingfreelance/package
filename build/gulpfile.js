var gulp             = require('gulp'),
    sass             = require('gulp-sass'),
    autoprefixer     = require('gulp-autoprefixer'),
    sourcemaps       = require('gulp-sourcemaps'),
    minifyCss        = require('gulp-clean-css'),
    rename           = require('gulp-rename'),
    browserSync      = require('browser-sync').create();



    gulp.task('sass', function(){
        // sass directory
        return gulp.src('./sass/*scss')
                .pipe(sass())
                //outputstyle (nested, compact, expanded, compressed)
                .pipe(sass({outputStyle:'compact'}).on('error', sass.logError))
                // sourcemaps
                .pipe(sourcemaps.init())
                // sourcemaps output directory
                .pipe(sourcemaps.write(('./maps')))
                // css output directory
                .pipe(gulp.dest('../assets/css'));
    });



// gulp default (sass, minify-css, browser-sync) method
gulp.task('sass-build', ['sass']);
